import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comentarios } from '../modelos/comentarios';
@Injectable({
  providedIn: 'root'
})
export class ComentariosService {

  constructor(private http:HttpClient) { }

  jalaLaData(){
    return this.http.get<Observable<Comentarios>>('https://jsonplaceholder.typicode.com/comments');

  }


}


