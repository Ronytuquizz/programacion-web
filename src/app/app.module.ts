import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IndexComponent } from './menu/index/index.component';
import { FooterComponent } from './footers/footer/footer.component';
import { HttpClientModule } from '@angular/common/http'
import { ComentariosService } from './servicios/comentarios.service';
import { UsComponent } from './contenido/us/us.component';
import { ServicesComponent } from './contenido/services/services.component';
import { ClientsComponent } from './contenido/clients/clients.component';
import { ContactsComponent } from './contenido/contacts/contacts.component';
import {Routes,RouterModule} from '@angular/router';
import { DoorComponent } from './contenido/door/door.component';
const path: Routes = [{path:'ejemplo/nosotros',component:UsComponent},
                      {path:'ejemplo/servicios',component:ServicesComponent},
                      {path:'ejemplo/clientes',component:ClientsComponent},
                      {path:'ejemplo/contacto',component:ContactsComponent},
                      {path:'ejemplo/door',component:DoorComponent} ];


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    FooterComponent,
    UsComponent,
    ServicesComponent,
    ClientsComponent,
    ContactsComponent,
    DoorComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(path,{enableTracing:true})
  ],
  providers: [ComentariosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
